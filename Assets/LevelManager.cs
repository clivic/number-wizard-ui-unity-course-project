﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public void LoadScene(string name)
	{
		string n = name.Trim ();
		Application.LoadLevel (n);
		Debug.Log (string.Format ("Level {0} successfully loaded.", n));
	}

	public void QuitGame()
	{
		Application .Quit ();
		Debug.Log ("Game quited.");
	}

}
