﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NumberWizards : MonoBehaviour
{
	public Text TbGuess;
	public Text LblHeader;
	public Text LblGuessPrefix;
	public Text LblGuessPostfix;
	public LevelManager levelManager;
	public Cfg cfg;

	int max;
	int min;
	int guess;
	int maxGuessesAllowed;
	
	// Use this for initialization
	void Start ()
	{
		LoadCfg ();
		StartGame ();
	}

	void LoadCfg ()
	{
		cfg = Cfg.Instance;
		max = cfg.Max;
		min = cfg.Min;
		maxGuessesAllowed = cfg.MaxGuessAllowed;
	}

	void StartGame ()
	{
		LblHeader.text = string.Format(
@"Welcome to Number Wizard.
Pick a number in your head, but don't tell me.
Your goal is to fail me to guess right within {2} times.
The highest number you can pick is {0};
The lowest number you can pick is {1}.", max, min, maxGuessesAllowed);

		NextGuess (true);
//		print ("Is the number higher or lower than " + guess + "?");
//		print ("Up arrow = higher, Down = lower, and return = equal.");
		
	}
	
	void NextGuess (bool firstGuess = false)
	{
		// guess = (max + min) / 2;
		guess = Random.Range (min, max + 1);

		if (firstGuess)
			LblGuessPrefix.text = "I guess ";
		else
		{
			int r = Random.Range(0,6);
			switch (r)
			{
			case 0 :LblGuessPrefix.text = "Ok. Then I guess ";break;
			case 1 :LblGuessPrefix.text = "Well... I guess ";break;
			case 2 :LblGuessPrefix.text = "Umm... This time I guess ";break;
			case 3 :LblGuessPrefix.text = "Ah, this is gonna be a bit hard... I guess ";break;
			case 4 :LblGuessPrefix.text = "Fine. I guess ";break;
			default :LblGuessPrefix.text = "I guess ";break;
			}
		}
		TbGuess.text = guess.ToString ();
		if (firstGuess)
			LblGuessPostfix.text = "Did I get it?";
		else 
		{
			int r = Random.Range(0,6);
			switch (r)
			{
			case 0: LblGuessPostfix.text = "This time am I right?"; break;
			case 1: LblGuessPostfix.text = "How about this?"; break;
			case 2: LblGuessPostfix.text = "Did I get it right?"; break;
			case 3: LblGuessPostfix.text = "What about this time?"; break;
			case 4: LblGuessPostfix.text = "This time am I right?"; break;
			default: LblGuessPostfix.text = "How about it?"; break;				
			}
		}

		maxGuessesAllowed -= 1;
		LblGuessPostfix.text += 
			System.Environment.NewLine + string.Format ("I have {0} times left to guess.", maxGuessesAllowed);
		if (maxGuessesAllowed <= 0) 
		{
			// Computer loses for it failed to guess within the max times.
			levelManager.LoadScene("Win");
		}
	}

	public void GuessHigher ()
	{
		min = guess;
		NextGuess ();
	}

	public void GuessLower ()
	{
		max = guess;
		NextGuess ();
	}
}
