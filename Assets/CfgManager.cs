﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CfgManager : MonoBehaviour {
	public Cfg cfg = Cfg.Instance;

	public InputField MaxUI;
	public InputField MinUI;
	public Text MaxGuessAllowedUI;
	public Slider MaxGuessAllowedSlider;
	public Text ErrMsg;		
	
	public LevelManager levelManager;
	
	// Use this for initialization
	void Start ()
	{
		LoadCfg ();
	}

	public void LoadCfg ()
	{
		MaxUI.text = cfg.Max.ToString ();
		MinUI.text = cfg.Min.ToString ();
		MaxGuessAllowedSlider.value = cfg.MaxGuessAllowed;
		ErrMsg.text = cfg.ErrMsg;
		OnSliderValueChanged ();
	}

	public void SaveCfg ()
	{
		ErrMsg.text= cfg.SaveCfg(MaxUI.text, MinUI.text, MaxGuessAllowedSlider.value); 
		if(ErrMsg.text.Trim()==string.Empty)
			levelManager.LoadScene ("Start");
	}

	public void OnSliderValueChanged ()
	{
		int value = System.Convert.ToInt32(MaxGuessAllowedSlider.value);
		MaxGuessAllowedUI.text = value.ToString ();
	}
}

public class Cfg {
	protected static Cfg instance = null;

	protected int max = 1000;
	protected int min = 1;
	protected int maxGuessAllowed = 5;
	public string ErrMsg = string.Empty;
	public const int LMT_MAX = 9999;
	public const int LMT_MIN = 1;

	public int Max { get { return max; } }
	public int Min { get { return min; } }
	public int MaxGuessAllowed { get { return maxGuessAllowed; } }

	public delegate void DelOnCfgSaved();

//	public event DelOnCfgSaved OnCfgSaved = null;			

	protected Cfg(){}

	public static Cfg Instance
	{
		get
		{
			if(instance==null)
			{
				instance = new Cfg();
//				Object.DontDestroyOnLoad(Cfg.instance);
			}
			return instance;
		}	
	}

	public string SaveCfg(string maxStr, string minStr, float maxGuessAllowedValue)
	{
		ErrMsg = string.Empty;
		int maxToSave, minToSave, maxGuessToSave;
		try 
		{
			maxToSave = System.Convert.ToInt32(maxStr);
			if(maxToSave>LMT_MAX) throw new System.Exception();
		} 
		catch (System.Exception ex) 
		{
			Debug.LogError(ex.Message);
			ErrMsg= string.Format("Max guess is invalid. Please enter a number less than {0}.",LMT_MAX);return ErrMsg;
		}
		try 
		{
			minToSave = System.Convert.ToInt32(minStr);
			if(minToSave<LMT_MIN) throw new System.Exception();
		} 
		catch (System.Exception ex) 
		{
			Debug.LogError(ex.Message);
			ErrMsg = string.Format("Min guess is invalid. Please enter a number greater than {0}.",LMT_MIN);return ErrMsg;
		}
		if (maxToSave <= minToSave) 
		{
			ErrMsg = string.Format("Min guess shall be less than max guess. Please check."); return ErrMsg;
		}
		try 
		{
			maxGuessToSave = System.Convert.ToInt32(maxGuessAllowedValue);
		} 
		catch (System.Exception ex) 
		{
			Debug.LogError(ex.Message);
			ErrMsg = string.Format("Fail to convert slider value to integer. Please check."); return ErrMsg;
		}
		max = maxToSave;
		min = minToSave;
		maxGuessAllowed = maxGuessToSave;
		return ErrMsg;
	}
}
